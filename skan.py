import cv2
import grabscreen as gs
import win32api, win32con
import getkeys as gk
import time
import numpy as np

CENTER = [362, 242]


def region_of_interest(img):
    poly = np.array([
        [(350, 340), (350, 336), (354, 336), (354, 340)]
    ])
    mask = np.zeros_like(img)
    cv2.fillPoly(mask, poly, 255)
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image


def leftClick():
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN,0,0)
    time.sleep(.1)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP,0,0)
    print('BOOSTED!')


def find_circles(img_target: np.ndarray, img_source: np.ndarray) -> tuple:
    circles = cv2.HoughCircles(img_source, cv2.HOUGH_GRADIENT, 1, 3,
                               param1=150,
                               param2=7,
                               minRadius=1,
                               maxRadius=5)

    if circles is not None:

        # circles shape - number of circles, (x, y, radius)
        circles = np.round(circles[0, :]).astype('int')
        x1 = circles[:, 0]
        y1 = circles[:, 1]
        x2, y2 = CENTER

        distance = distance_points(x1, x2, y1, y2)

        # index min distance
        index = np.where(distance == np.min(distance))[0][0]

        # display line from center to target point
        cv2.line(img_target, (x1[index], y1[index]), (x2, y2), (255, 0, 0), 2)

        # display of all points on the original image
        for (x, y, r) in circles:
            cv2.circle(img_target, (x, y), r, (0, 255, 0), cv2.FILLED)
            cv2.putText(img_target, 'food!', (x - 10, y - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 200, 200), 1)


        return x1[index], y1[index]


def distance_points(x1, x2, y1, y2):
    # distance = sqrt((x1 - x2)^2+(y1-y2)^2)
    elem1 = np.power((x2 - x1), 2)
    elem2 = np.power((y2 - y1), 2)
    distance = np.round(np.sqrt((elem1 + elem2)))
    return distance


def distance_enemy(x1, x2, y1, y2):
    distance = np.sqrt(np.power((x1 - x2), 2) + np.power((y1 + 95 - y2), 2))
    return distance


def find_snakes(img_target, img_source):
    x2, y2 = CENTER

    cv2.rectangle(img_source, (x2 - 80, y2 + 80), (x2 + 80, y2 - 80), (0, 0, 0), cv2.FILLED)

    # считываем значения бегунков
    h1 = cv2.getTrackbarPos('min', 'settings')
    s1 = cv2.getTrackbarPos('max', 'settings')
    v1 = cv2.getTrackbarPos('d', 'settings')

    blur = cv2.GaussianBlur(img_source, (5, 5), 0)
    # canny_img = cv2.Canny(blur, 50, 150)
    ret, thresh_delta = cv2.threshold(blur, 95, 255, cv2.THRESH_BINARY)
    thresh_delta = cv2.dilate(thresh_delta, None, iterations=0)
    (cnts, _) = cv2.findContours(thresh_delta.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    center = []
    enemy = []

    x_cent = []
    y_cent = []
    BOOSTED = False
    centroid = (0, 0)

    for contour in cnts:

        if cv2.contourArea(contour) < 228:
            continue
        (x, y, w, h) = cv2.boundingRect(contour)
        cv2.rectangle(img_target, (x, y), (x + w, y + h), (0, 0, 255), 3)
        cv2.putText(img_target, 'Snake', (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        center_x, center_y = np.round(w / 2 + x).astype('int'), np.round(h / 2 + y).astype('int')
        center.append((center_x, center_y))
        cv2.line(img_target, (center_x, center_y), (x2, y2), (255, 0, 0), 2)

        # if the enemy is in the player's area of action, the player boosted
        distance = distance_enemy(center_x, x2, center_y, y2)
        if distance < 400:
            print(distance)
            enemy.append((center_x, center_y))
            x_cent.append(center_x)
            y_cent.append(center_y)
            if distance < 200:
                BOOSTED = True
        else:
            BOOSTED = False
            enemy.append((0, 0))

    if len(x_cent) > 0:
        centroid = np.round((np.sum(x_cent) / len(x_cent), np.sum(y_cent) / len(y_cent))).astype('uint16')
        cv2.line(img_target, (CENTER[0], CENTER[1]), (centroid[0], centroid[1]), (42, 129, 249), 2)

    if len(enemy) == 0:
        enemy.append((0, 0))
    if len(center) > 0:
        return center, enemy, centroid, BOOSTED


def main():
    global enemy, keys

    def nothing(*arg):
        pass

    cv2.namedWindow("result")  # create main window
    cv2.namedWindow("settings")  # create window settings

    # create track bar
    cv2.createTrackbar('min', 'settings', 0, 255, nothing)
    cv2.createTrackbar('max', 'settings', 0, 255, nothing)
    cv2.createTrackbar('d', 'settings', 0, 1000, nothing)

    paused = False
    BOOSTED = False
    previos_frame = None
    centroid = 0, 0
    while True:

        if not paused:

            screen = gs.grab_screen(region=(0, 100, 720, 576))
            img = np.copy(screen)
            img2 = np.copy(screen)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            if previos_frame is None:
                previos_frame = img
                continue

            gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(gray, (15, 15), 0)
            ret, thresh1 = cv2.threshold(blur, 80, 255, cv2.THRESH_BINARY)

            canny_img = cv2.Canny(thresh1, 30, 150)
            cv2.circle(canny_img, (361, 242), 12, (0, 0, 0), cv2.FILLED)

            try:
                target_x, target_y = find_circles(img2, canny_img)
                print(target_x, target_y)
            except TypeError:
                target_x, target_y = CENTER

            try:
                centers_snake, enemy, centroid, BOOSTED = find_snakes(img2, img)
            except TypeError:
                pass

            if enemy[0] != (0,0):
                print(enemy)
                if centroid[0] > CENTER[0]:
                    target_x = CENTER[0] - 50
                else:
                    target_x = CENTER[0] + 50
                if centroid[1] > CENTER[1]:
                    target_y = CENTER[1] - 50
                else:
                    target_y = CENTER[1] + 50
                win32api.SetCursorPos((target_x, target_y + 95))
                if BOOSTED:
                    leftClick()
                    cv2.putText(img2, 'BOOSTED!', (CENTER[0]-10, CENTER[1] - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 200, 200), 2)
            else:
                win32api.SetCursorPos((target_x, target_y + 95))

            # show images
            cv2.imshow('bin', img)
            cv2.imshow('img canny', img2)

            if cv2.waitKey(25) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                break
            keys = gk.key_check()

        if 'T' in keys:
            if paused:
                paused = False
                time.sleep(5)
            else:
                print('Paused!')
                paused = True


if __name__ == '__main__':
    main()
