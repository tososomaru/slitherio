import directkeys as dk
import time
import random

t_time = 0.06

def up():
    print('up')
    dk.PressKey(dk.W)
    dk.ReleaseKey(dk.S)
    dk.ReleaseKey(dk.A)
    dk.ReleaseKey(dk.D)


def down():
    if random.randrange(0, 2) == 1:
        dk.PressKey(dk.A)
        print('down and left')
    else:
        dk.PressKey(dk.D)
        print('down and right')
    dk.PressKey(dk.S)
    dk.ReleaseKey(dk.W)
    dk.ReleaseKey(dk.A)
    dk.ReleaseKey(dk.D)


def not_key():
    print('not key')
    if random.randrange(0, 3) == 1:
        dk.PressKey(dk.W)
    else:
        dk.ReleaseKey(dk.W)
    dk.ReleaseKey(dk.S)
    dk.ReleaseKey(dk.A)
    dk.ReleaseKey(dk.D)


def left():
    print('left')
    # if random.randrange(0, 2) == 1:
    #     dk.PressKey(dk.W)
    # else:
    #     dk.ReleaseKey(dk.W)
    dk.PressKey(dk.W)
    dk.PressKey(dk.A)
    dk.ReleaseKey(dk.S)
    dk.ReleaseKey(dk.D)
    time.sleep(t_time)
    dk.ReleaseKey(dk.A)


def right():
    print('right')
    # if random.randrange(0, 2) == 1:
    #     dk.PressKey(dk.W)
    # else:
    #     dk.ReleaseKey(dk.W)
    dk.PressKey(dk.W)
    dk.PressKey(dk.D)
    dk.ReleaseKey(dk.S)
    dk.ReleaseKey(dk.A)
    time.sleep(t_time)
    dk.ReleaseKey(dk.D)


def up_left():
    dk.PressKey(dk.W)
    dk.PressKey(dk.A)
    print('up and left')
    dk.ReleaseKey(dk.S)
    dk.ReleaseKey(dk.D)
    time.sleep(t_time)
    dk.ReleaseKey(dk.A)


def up_right():
    dk.PressKey(dk.W)
    dk.PressKey(dk.D)
    print('up and right')
    dk.ReleaseKey(dk.S)
    dk.ReleaseKey(dk.A)
    time.sleep(t_time)
    dk.ReleaseKey(dk.D)


def down_left():
    dk.PressKey(dk.S)
    dk.PressKey(dk.A)
    print('down and left')
    dk.ReleaseKey(dk.W)
    dk.ReleaseKey(dk.D)


def down_right():
    dk.PressKey(dk.S)
    dk.PressKey(dk.D)
    print('down and right')
    dk.ReleaseKey(dk.W)
    dk.ReleaseKey(dk.A)
